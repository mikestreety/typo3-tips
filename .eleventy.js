
const eleventyNavigationPlugin = require('@11ty/eleventy-navigation');

module.exports = function (config) {
    config.addPlugin(eleventyNavigationPlugin);
    config.addFilter('squash', require('./app/filters/squash.js'));
    config.addFilter('weed', require('./app/filters/weed-out.js'));

    return {
        dir: {
            input: 'app/content',
            output: 'html',

            data: './../data',
            includes: './../includes',
            layouts: './../layouts'
        }
    };
};