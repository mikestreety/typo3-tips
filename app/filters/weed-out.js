/**
 * Make a search index string by removing duplicated words
 * and removing less useful, common short words
 *
 * @param {String} text
 */

module.exports = function (text) {

    var content = new String(text);

    //remove newlines, and punctuation
    content = content.replace(/\\/gi, ' ');
    content = content.replace(/\(\)/gi, ' ');
    content = content.replace(/::/gi, ' ');
    content = content.replace(/\t/gi, ' ');
    content = content.replace(/[ ]{2,}/g, ' ');

    return content;
}