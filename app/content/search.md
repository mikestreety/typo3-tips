# Search

<div id="results"></div>

<script>
const queryString = new URLSearchParams(window.location.search),
		search = queryString.get('q').trim().toLowerCase();

if(!search.length) {
		// do soething
}

let searchData = fetch('/search.json').then(data => data.json())
	.then(data => {
		let results = [];
		for(let item of data) {

			let result = {
					points: 0,
					item
				},
				s = item.search,
				regex = new RegExp(search, 'g');

			if(s.title.includes(search)) {
				result.points += s.title.match(regex).length * 100;
			}

			if(s.text.includes(search)) {
				result.points += s.text.match(regex).length;
			}

			if(result.points) {
				results.push(result);
			}
		}

		return results.sort(
			(a, b) => (a.points > b.points) ? 1 : -1
		);
	});

searchData.then(data => {
	let results = document.getElementById('results');

	results.innerHTML = '';

	if(data.length) {
		list = document.createElement('ol');
		for (let result of data) {
			let childElement = document.createElement('li');
			childElement.innerHTML = `
				<h2>
					<a href="${result.item.url}">
						${result.item.display.title}
					</a>
				</h2>
			`;
			list.appendChild(childElement)
		}
	}

	results.appendChild(list);
})
</script>

