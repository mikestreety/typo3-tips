---
title: wrapScriptTags
eleventyNavigation:
  parent: Deprecations
  key: wrapScriptTags
---

TYPO3 have deprecated `wrapScriptTags`, however a `GeneralUtility` static function works the same

The helper script will `grep` for `wrapScriptTags`.

### Problem

    Call to undefined method TYPO3\CMS\Backend\Template\DocumentTemplate::wrapScriptTags()


### Solution

Find all `$this->doc->wrapScriptTags` and replace with `GeneralUtility::wrapJS`. Ensure `use TYPO3\CMS\Core\Utility\GeneralUtility;` is located at the top of the file.
