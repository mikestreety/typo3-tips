---
title: extRelPath
eleventyNavigation:
  parent: Deprecations
  key: extRelPath
---

TYPO3 have deprecated `extRelPath`, however `extPath` works the same.

The helper script will `grep` for `extRelPath`.

### Problem

    Call to undefined method TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath()


### Solution

Replace `extRelPath()` with `extPath()`