---
title: No extension key could be found
eleventyNavigation:
  parent: Deprecations
  key: No extension key
---

### Problem

<pre>(1/1) #1404068038 InvalidArgumentException
No extension key could be determined when calling addPlugin()! This method is meant to be called from Configuration/TCA/Overrides files. The extension key needs to be specified as third parameter. Calling it from any other place e.g. ext_localconf.php does not work and is not supported.</pre>

### Solution

The key here is **The extension key needs to be specified as third parameter.**.

Find the addPlugin and add a third parameter of what it is, without the PI or underscores.

For example [This would have an extra parameter](https://gitlab.lldev.co.uk/ll/sw/blob/09b2dc0354f63a7a62258aea7e45acec7a3f22f3/html/typo3conf/ext/sw/ext_tables.php#L10-17) of `swpromos` after `list_type`:

**From:**

<pre class="language-php">\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
	[
		'Promos',
		'sw_promos_pi',
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('sw').'Plugin/Promos/Assets/Plugin.gif'
	],
	'list_type'
);</pre>

**To:**

<pre class="language-php">\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
	[
		'Promos',
		'sw_promos_pi',
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('sw').'Plugin/Promos/Assets/Plugin.gif'
	],
	'list_type',
	'swpromos'
);</pre>

