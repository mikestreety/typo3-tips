---
title: Remove filenames on class call
eleventyNavigation:
  parent: Deprecations
  key: Remove filenames on class call
---

## Problem

TYPO3 have taken away the requirement for files to be specified when calling classes.

<pre>(1/1) Error
Class 'EXT:path/to.file.php:tx_class_name' not found</pre>

## Solution

Wherever `filename:class` is called, remove the filename from the path and run `dumpautoload`

e.g.

From:

<pre>EXT:plumpton/Hook/Survey.php:tx_plumpton_hook_survey</pre>

To:

<pre>tx_plumpton_hook_survey</pre>